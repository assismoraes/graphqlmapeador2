package br.com.wildrimak.services;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.wildrimak.models.Field;
import br.com.wildrimak.models.Key;
import br.com.wildrimak.models.KeyType;
import br.com.wildrimak.models.Table;
import br.com.wildrimak.models.TypeFieldEnum;

@Component
public class TablesService {

	@Autowired
	protected DataSource dataSource;

	private String nomeBanco(ResultSet tables) throws SQLException {
//		String nome = tables.getString(2);
//		System.out.println(nome);
		String nome = "";
		return nome;
	}

	private String nomeTabela(ResultSet tables) throws SQLException {
		String nome = tables.getString("TABLE_NAME");
		return nome;
	}

	private int quantidadeColunas(ResultSet tables) throws SQLException {

		ResultSetMetaData resultSetMetaData = tables.getMetaData();
		int count = resultSetMetaData.getColumnCount();
		return count;
	}

	private List<String> chavesPrimarias(ResultSet columnsPk) throws SQLException {

		System.out.println("EU SOU: " + columnsPk.equals(null));
		List<String> chavesPrimarias = new ArrayList<>();

		while (columnsPk.next()) {
			System.out.println("MINHA COLUNA É: " + columnsPk.getString(4));
			String pkColumnName = columnsPk.getString(4);
			chavesPrimarias.add(pkColumnName);
		}

		return chavesPrimarias;
	}

	private List<Key> chavesEstrangeiras(ResultSet columnsFk) throws SQLException {

		List<Key> chavesEstrangeiras = new ArrayList<>();

		while (columnsFk.next()) {

			String nomeColuna = columnsFk.getString(4);
			/*Possibilidade de ser nome da tabela origem: columnsFk.getString(7);*/
			String tipoColuna = columnsFk.getString(7);
			String nomeTabela = columnsFk.getString(3);
			Table table = new Table(nomeTabela);
			System.out.println(nomeColuna);
			System.out.println("O MEU TIPO É: " + tipoColuna);
			System.out.println(nomeTabela);
			TypeFieldEnum tipo = findTypeByString(tipoColuna);
			Field field = new Field(table, nomeColuna, tipo, KeyType.FOREIGN_KEY);

			String tabelaOrigem = "A DESCOBRIR";

			Key key = new Key(KeyType.FOREIGN_KEY, field, tabelaOrigem);

			chavesEstrangeiras.add(key);
		}

		return chavesEstrangeiras;
	}

	private List<String> nomeColunas(ResultSet columnsAll) throws SQLException {
		return null;
	}

	private List<String> nomeETipoColunas(ResultSet columnsAll) throws SQLException {
		return null;
	}

	private List<String> nomeTipoTamanhoColunas(ResultSet columnsAll) throws SQLException {
		return null;
	}

	private void imprima(DatabaseMetaData dbMetaData) throws Exception {

		ResultSet tables = dbMetaData.getTables(null, null, null, new String[] { "TABLE" });
		String nomeDoBanco = this.nomeBanco(tables);

		System.out.println("\nNome do banco de dados: " + nomeDoBanco);

		while (tables.next()) {

			String tableName = this.nomeTabela(tables);
			ResultSet columnsPk = dbMetaData.getPrimaryKeys(null, null, tableName);
			ResultSet columnsFk = dbMetaData.getExportedKeys(null, null, tableName);
			ResultSet columnsAll = dbMetaData.getColumns(null, null, tableName, "%");
			int qtdColunas = this.quantidadeColunas(tables);

			System.out.println("\tNome da tabela: " + tableName);
			System.out.println("\tQtd de colunas: " + qtdColunas);

			for (String chavePrimaria : this.chavesPrimarias(columnsPk)) {
				System.out.println("\t\tColuna Chave Primaria: " + chavePrimaria);
			}

			for (Key chaveEstrangeira : this.chavesEstrangeiras(columnsFk)) {
				System.out.println("\t\tColuna Chave Estrangeira: " + chaveEstrangeira);
				System.out.println("\t\t\tTabela de Origem: ");
			}

			System.out.println("\n\t\tEsquema dos campos:\n");

			while (columnsAll.next()) {
				System.out.println("\t\tNome: ");
				System.out.println("\t\tTipo: ");
				System.out.println("\t\tTamanho: ");
			}

		}

	}

	public List<Table> showTables() throws Exception {

		DatabaseMetaData metaData = dataSource.getConnection().getMetaData();
		imprima(metaData);

		ResultSet tables = metaData.getTables(null, null, null, new String[] { "TABLE" });
		List<Table> myTables = new ArrayList<>();

		while (tables.next()) {

			String tableName = tables.getString("TABLE_NAME");
			Table newTable = new Table(tableName);
			ResultSet columns = metaData.getColumns(null, null, tableName, "%");
			ResultSet keys = metaData.getPrimaryKeys(null, null, tableName);
			metaData.getExportedKeys(null, null, tableName);
			while (columns.next()) {
				//String columnName = columns.getString("COLUMN_NAME");
//				String typeName = columns.getString("TYPE_NAME");
//				int typeName2 = columns.getType();
//				System.out.println("O valor é: " + typeName2);
				String nomeDaTabela = columns.getString(3);
				String colunaNome = columns.getString(4);
				String colunaTipo = columns.getString(6);
				String colunaTamanho = columns.getString(7);
				String colunaTestada = columns.getString(16);
				String Imprima = "\nNome da tabela: " + nomeDaTabela + "" + "\nColuna nome: " + colunaNome + ""
						+ "\nColuna tipo: " + colunaTipo + "" + "\nColuna tamanho: " + colunaTamanho + ""
						+ "\nColuna testada: " + colunaTestada;
				System.out.println(Imprima);
				TypeFieldEnum tipo = findTypeByString(colunaTipo);
				newTable.addField(new Field(newTable, colunaNome, tipo));
			}

			myTables.add(newTable);
		}

		for (Table table : myTables) {
			System.out.println(table);
		}

		return myTables;
	}
	
	private TypeFieldEnum findTypeByString(String colunaTipo) {
		TypeFieldEnum type = TypeFieldEnum.STRING;
		System.out.println("MEU TIPO ORIGINAL É: " + colunaTipo);
		return type;
	}
}
