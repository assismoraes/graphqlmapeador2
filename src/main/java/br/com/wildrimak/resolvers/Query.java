package br.com.wildrimak.resolvers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import br.com.wildrimak.models.Table;
import br.com.wildrimak.services.TablesService;

public class Query implements GraphQLQueryResolver {

	@Autowired
	private TablesService tableService;

	public Query() {
	}
	
	public List<Table> tables() {
		try {
			return tableService.showTables();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
