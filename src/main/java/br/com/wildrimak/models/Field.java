package br.com.wildrimak.models;

public class Field {

	private String name;
	private TypeFieldEnum type;
	private Key key;
	private Table table;
	
	private Field() {
		Table pattern = new Table("Não sou uma tabela");
		this.table = pattern;
	}

	public Field(Table table) {
		this(); //Agora field não é nulo
		System.out.println("Const" + this.equals(null));
		this.key = new Key(KeyType.NOT_KEY, this);
		System.out.println("Construção: " + name + " " + type);
	}

	public Field(Table table, String name) {
		this(table);
		this.name = name;
	}

	public Field(Table table, String name, TypeFieldEnum type) {
		this(table, name);
		this.type = type;
	}

	public Field(Table table, String name, TypeFieldEnum type, KeyType keyType) {
		this(table, name, type);
		this.key = new Key(keyType, this);
	}

	public String getName() {
		return name;
	}

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TypeFieldEnum getType() {
		return type;
	}

	public void setType(TypeFieldEnum type) {
		this.type = type;
	}

	public boolean isPrimaryKey() {

		if (key.getType() == KeyType.PRIMARY_KEY) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isForeignKey() {

		if (key.getType() == KeyType.FOREIGN_KEY) {
			return true;
		} else {
			return false;
		}

	}

}
