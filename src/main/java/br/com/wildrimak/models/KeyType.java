package br.com.wildrimak.models;

public enum KeyType {
	PRIMARY_KEY, FOREIGN_KEY, NOT_KEY
}
