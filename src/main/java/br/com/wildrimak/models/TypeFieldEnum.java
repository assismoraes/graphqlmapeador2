package br.com.wildrimak.models;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public enum TypeFieldEnum {

	BIGDECIMAL("BigDecimal", BigDecimal.class), BYTE("Byte", Byte.class), BOOLEAN("Boolean", Boolean.class),
	CHARACTER("Character", Character.class), DATE("Date", Date.class), DOUBLE("Double", Double.class),
	FLOAT("Float", Float.class), INTEGER("Integer", Integer.class), LONG("Long", Long.class),
	SHORT("Short", Short.class), STRING("String", String.class);

	private String description;
	private Class<?> klass;
	private static Map<String, Class<?>> typesMap;

	static {
		typesMap = new HashMap<>();
		defineTypes();
	}

	TypeFieldEnum(String description, Class<?> klass) {
		this.description = description;
		this.klass = klass;
	}

	public String getDescription() {
		return description;
	}

	public Class<?> getKlass() {
		return klass;
	}

	public void setKlass(Class<?> klass) {
		this.klass = klass;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription(TypeFieldEnum typeFieldEnum) {
		for (TypeFieldEnum type : values()) {
			if (type.equals(typeFieldEnum))
				return type.description;
		}
		return null;
	}

	public Map<String, Class<?>> getTypesMap() {
		return typesMap;
	}

	public static Class<?> translateTypeDatabaseToJava(String nameFieldDatabase) {

		Class<?> klass = typesMap.get(nameFieldDatabase);

		if (klass.equals(null)) {
			klass = Object.class;
		}

		return klass;
	}

	private static void defineTypes() {

		setTextualValues();
		setNumericalValues();
		setConditionalValues();
		setTimeValues();
		setOtherValues();

	}

	private static void setTextualValues() {

		typesMap.put("text", String.class);
		typesMap.put("character", String.class);
		typesMap.put("char", String.class);
		typesMap.put("character varying", String.class);
		typesMap.put("varchar", String.class);

	}

	private static void setNumericalValues() {

		typesMap.put("smallint", Byte.class);
		typesMap.put("integer", Integer.class);
		typesMap.put("int", Integer.class);
		typesMap.put("int2", Integer.class);
		typesMap.put("int4", Integer.class);
		typesMap.put("int8", BigInteger.class);
		typesMap.put("bigint", BigInteger.class);

		typesMap.put("smallserial", Byte.class);
		typesMap.put("serial", Integer.class);
		typesMap.put("serial2", Integer.class);
		typesMap.put("serial4", Integer.class);
		typesMap.put("serial8", BigInteger.class);
		typesMap.put("bigserial", BigInteger.class);

		typesMap.put("float4", Float.class);
		typesMap.put("float8", BigDecimal.class);

		typesMap.put("money", BigDecimal.class);
		typesMap.put("double precision", BigDecimal.class);
		typesMap.put("real", BigDecimal.class);

	}

	private static void setTimeValues() {

		typesMap.put("time", Date.class);
		typesMap.put("timetz", Date.class);
		typesMap.put("timestamp", Date.class);
		typesMap.put("timestamptz", Date.class);
		typesMap.put("date", Date.class);

	}

	private static void setConditionalValues() {

		typesMap.put("boolean", Boolean.class);
		typesMap.put("bool", Boolean.class);

	}

	private static void setOtherValues() {

		typesMap.put("json", Object.class);
		typesMap.put("xml", Object.class);
		typesMap.put("inet", Object.class);
		typesMap.put("line", Object.class);
		typesMap.put("lseg", Object.class);
		typesMap.put("macaddr", Object.class);
		typesMap.put("path", Object.class);
		typesMap.put("point", Object.class);
		typesMap.put("polygon", Object.class);
		typesMap.put("tsquery", Object.class);
		typesMap.put("tsvector", Object.class);
		typesMap.put("txid_snapshot", Object.class);
		typesMap.put("uuid", Object.class);
		typesMap.put("varbit", Object.class);
		typesMap.put("box", Object.class);
		typesMap.put("bytea", Object.class);
		typesMap.put("cidr", Object.class);
		typesMap.put("circle", Object.class);

	}

}
