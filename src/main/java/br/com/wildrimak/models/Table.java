package br.com.wildrimak.models;

import java.util.ArrayList;
import java.util.List;

public class Table {

	private String name;
	private String type = "Default";
	private List<Field> fields;

	public Table() {
		this.type = "Default";
		this.fields = new ArrayList<Field>();
	}

	public Table(String name) {
		this();
		setName(name);
	}

	public Table(String name, List<Field> fields) {
		this(name);
		this.fields = fields;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;

		if (name.startsWith("dim_")) {
			this.type = "dimension";
		} else if (name.startsWith("fact_")) {
			this.type = "fact";
		} else {
			this.type = "default";
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public void addField(Field field) {
		this.fields.add(field);
	}

	@Override
	public String toString() {

		String tableName = getName();

		for (Field field : fields) {
			tableName += "\n\t" + field.getName() + " : " + field.getType();
		}

		return tableName;
	}

}
