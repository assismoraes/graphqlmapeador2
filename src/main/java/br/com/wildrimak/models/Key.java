package br.com.wildrimak.models;

public class Key {

	private KeyType type;
	private Field field;
	private String tabelaOrigem;

	public Key(KeyType type, Field field) {
		this.type = type;
		this.field = field;

		if (type == KeyType.PRIMARY_KEY) {
			this.tabelaOrigem = field.getTable().getName();
		} else if (type == KeyType.NOT_KEY) {
			this.tabelaOrigem = field.getTable().getName();
		} else {
			this.tabelaOrigem = "NÃO DEFINIDO";
		}

	}

	public Key(KeyType type, Field field, String tabelaOrigem) {
		this(type, field);
		this.tabelaOrigem = tabelaOrigem;
	}

	public KeyType getType() {
		return type;
	}

	public void setType(KeyType type) {
		this.type = type;
	}

	public String getTabelaOrigem() {
		return tabelaOrigem;
	}

	public void setTabelaOrigem(String tabelaOrigem) {
		this.tabelaOrigem = tabelaOrigem;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

}
