package br.com.wildrimak.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Depurador {

	private static int ordem = 0;
	private static final String path = "/home/wildrimak/logs/log.txt";

	public static void imprima(Object thisClass, Object string) {
		String canonicalName = thisClass.getClass().getCanonicalName();
		String formato = "\n" + canonicalName + ": " + string + "\n";
		imprima(formato);
	}

	public static void imprima(Object thisClass, Object... strings) {
		for (Object string : strings) {
			imprima(thisClass, string);
		}
	}

	private static void registreNoLog(Object string, String path) {

		FileWriter arq = null;

		try {
			arq = new FileWriter(path, true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		PrintWriter gravarArq = new PrintWriter(arq);
		gravarArq.printf(string + "%n");

		try {
			arq.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void imprima(Object string) {
		ordem++;
		String formato = "\n>> " + ordem + "º) " + string;
		registreNoLog(formato, path);
		System.out.println(formato);
	}
}
