package br.com.wildrimak.builders;

import br.com.wildrimak.models.Field;
import br.com.wildrimak.models.Key;
import br.com.wildrimak.models.Table;
import br.com.wildrimak.models.TypeFieldEnum;

public class BuilderField {
	
	private String name;
	private TypeFieldEnum type;
	private Key key;
	private Table table;
	
	public BuilderField withName(String name) {
		this.name = name;
		return this;
	}
	
	public BuilderField withType(TypeFieldEnum type) {
		this.type = type;
		return this;
	}
	public BuilderField withKey(Key key) {
		this.key = key;
		return this;
	}
	public BuilderField withTable(Table table) {
		this.table = table;
		return this;
	}
	
	public Field build() {
		return new Field(this.table, this.name, this.type, this.key.getType());
	}
	

}
