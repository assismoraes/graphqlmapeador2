package br.com.wildrimak.builders;
import java.util.List;

import br.com.wildrimak.models.Field;
import br.com.wildrimak.models.Table;

public class BuilderTable {
	
	private String name;
	private List<Field> fields;
	
	public BuilderTable withName(String name) {
		this.name = name;
		return this;
	}
	
	public BuilderTable addField(Field field) {
		this.fields.add(field);
		return this;
	}
	
	public Table build() {
		Table table = new Table(this.name, this.fields);
		return table;
	}

}
