package br.com.wildrimak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.wildrimak.resolvers.Query;

@SpringBootApplication
public class GraphQlMapeador2Application {

	public static void main(String[] args) {
		SpringApplication.run(GraphQlMapeador2Application.class, args);
	}
	
	@Bean
	public Query query() {
		return new Query();
	}

}
